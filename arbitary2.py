"""
def fun(name,email="test@domain.com"):

    print("name=",name)
    print("email",email)
#position function
fun("aman","ak@gmail.com")
#keyword arguments
fun(email="at@gmail.com",name="me. abcd")

#default argumnets(def ch pehla hi define kr deni a email)
fun("peter")
fun("peter","peter@gmail.com")
"""
#multiply two numbers
"""
def mul(a,b):
    print("result=",a*b)
mul(10,20)

#by using arguments multiply
def mul(*args):
    print(args)
mul()
mul(10)
mul(10,20)
"""
#multiplication by multiple arguments
"""
def mul(*abcd):
    m=1
    for i in abcd:
        m=m*i
    print("result:",m)    

mul(10)
mul(20,30)
mul(10,20,30,2,3,4,5,6,7,89)
"""
#arbitary number of keyword arguments
"""
def student(**abcd):
    print(abcd)
    
student(name="shinchan",surname="nohara",age=5)
student(name="nobita",marks=10)


def student(**abcd):
    for k,v in abcd.items():
        print(k,':',v)
    print("---------------")


student(name="shinchan",surname="nohara",age=5)
student(name="nobita",marks=10)

fields = int(input("number of fields:"))
dict={}
for i in range(fields):
    key= input("enter field name:")
    val = input("enter field name:")
    dict[key] = val

student()
"""
#lambda function

def square(a):
    return a**2

sq = lambda b:b**2
print(sq(100))

add = lambda x,y:x+y
print(add(0,1))

d = lambda st:st.isdigit()
print(d("1000"))
print(d("lhche"))