#filter
"""
marks=[100,20,30,40,59]

def check(v):
    if v>=33:
        return True

result=list(filter(check,marks))   
print(result)     
print("Total:",len(result))

result = list(filter(lambda v: (v>=33), marks))
print(result)
comp = [i for i in marks if i>=33]
print(comp)


#map function
ls= ["pRaBh","DaMan","BaLdEeP","PolO"]
def check(st):
      return st.title()

def perc(n):
    p=(n/120)*100
    return round(p,3)     
result= list(map(check,ls))
print(result)   

print(marks)
percentage=list(map(perc,marks))
print(percentage)

marks = print(list(map(lambda a:a.title(),ls)))
print(marks)
"""
# reduce function
from functools import reduce

marks = [100,23,4,44,456,4,7,8,798,4,344,3]

result = reduce(lambda x,y:x+y, marks)

total=len(marks)*100
print((result/total)*100)


max = reduce(lambda a,b: a if a>b else b, marks)
print(max)

min = reduce(lambda a,b: a if a<b else b, marks)
print(min)