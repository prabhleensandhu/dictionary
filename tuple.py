ls = [10,20,30,40,50]
tp = (100,90,80,60)

print(tp[1])
print(tp[-1])

print(tp[2:])
print(tp[:2])
print(tp[::1])
print(tp[::2])
print(tp[::3])

inp = ("keyboard","mouse","scanner")
op = ("cpu","scanner","speaker")

print(inp+op)
print(inp*2)
print("keyboard" in inp)
print("keyboard" in op)

#print(ls[1])
#ls[1]=90
#print(ls[1])

#print(tp[1])
#tp[1]=9

print(ls)
del ls[1]
print(ls)

## Access elements
games = ("PubG","cc","coc","ff")
for i in games:
    print(i)

for i in range(len(games)):
    print(i,'=>',games[i])

for index,value in enumerate(games):
    print(index,value)    



for index,value in enumerate(games,start=1):
    print(index,':',value)

