ab = {
    "Name" : "Harry Potter",
    "Age"  : 17,
    "Friends" : ["Hermoine","Ron","Ginnie","Neville"],
    "Hobbies": ("Magic","Parseltonge"),
    "Platform No.":9.74,
    "Muggle":False,
}
##indexing
#print(ab["Name"],type(ab["Name"]))
#print(ab["Friends"][1])
#print(ab["Muggle"])

##looping 
#for n,v in enumerate(ab["Friends"],1):
   # print(n,v)

#for i in ab:
    ##print(i)
   ## print(i,'=',ab[i])  

    #print(ab.values())
   # print(ab.items())  
     
for k,v in ab.items():
    print("key:{}:{}".format(k,v))    