"""matrix = [
    [10,20,30],
    [30,40,50],
    [70,70,80],
]
print(matrix[1][-1])

print(matrix)
for row in range(len(matrix)):
    for col in range(len(matrix[0])):
        print("matrix[{}][{}]: {}".format(row,col,matrix[row][col]))


        #list comprehension
ls=[]
for i in range(1,101): 
    if i%3==0:
        ls.append(i)

ab = [i if i%2==0 else "not divided by 2" for i in range(1,101)]
print(ab)

cd = [i if i%3==0 else 0 for i in range(1,101)]
print(cd)


# list comprehension for matrix
mat = [[0 for j in range(3)] for i in range(3)]
 
matrix= [
    [10,20,30],
    [40,50,60],
    [70,80,90],
] 
for i in matrix:
    print(i)
print("\n")

for i in mat:
    print(i)


#for transposee
for r in range(len(matrix)):
    for c in range(len(matrix[0])):
        mat[r][c]=mat[c][r]
print("\n")

for i in mat:
    print(i)
"""


X = [[10,20,30], 
    [40,50,60], 
    [70,80,90]] 
   
Y = [[9,8,7], 
    [6,5,4], 
    [3,2,1]] 
  
result = [[X[i][j] + Y[i][j]  for j in range (len(X[0]))] for i in range(len(X))] 
   
for r in result: 
    print(r)