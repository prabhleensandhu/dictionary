class student:
    branch="CSE"
    college="SBBSU"

    def information(self,name,rno,email):
        print("Branch", self.branch)
        print("Name: {} \n Roll No: {} \n Email: {}".format(name,rno,email))
        print("**************\n")

    def ab(self,v):
        rs =lambda x:x**2
        return rs(v)  

    def sq(self,val):
        def square(v):
            return v**2
        return square(val)      

ob = student()
ob1 =student()
ob2 = student()
print(ob.college,"({})".format(ob.branch)) 

ob.information("prabh",1,"abcd@gmail.com")
ob1.information("baldeep",3,"adddbcd@gmail.com")
ob2.information("prabal",2,"abceeed@gmail.com")
print(ob.ab(10))
print(ob.sq(100))