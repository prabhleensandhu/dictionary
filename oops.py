#creating class
class employee:
#data member
    branch="SE"
    company="abcd company"

    #member function
    def info():
        print("Member Function")

#creating object of a class
ob=employee

#acessing data member
print(ob.company) 
print(ob.branch)

#acessing member function
ob.info()       